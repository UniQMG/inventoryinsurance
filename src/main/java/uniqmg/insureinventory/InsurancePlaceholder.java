package uniqmg.insureinventory;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

public class InsurancePlaceholder extends PlaceholderExpansion {

    private InsureInventory plugin;
    public InsurancePlaceholder(InsureInventory plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister(){
        return true;
    }

    @Override
    public String getIdentifier() {
        return "inventoryInsurance";
    }

    @Override
    public String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }


    @Override
    public String onPlaceholderRequest(Player player, String identifier){
        if(player == null) return "";

        // %inventoryInsurance_insured%
        if (identifier.equals("insured")) {
            return plugin.isInsured(player) ? "true" : "false";
        }

        return null;
    }
}
