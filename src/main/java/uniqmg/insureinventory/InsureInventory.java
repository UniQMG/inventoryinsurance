package uniqmg.insureinventory;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public final class InsureInventory extends JavaPlugin implements Listener {
    private Economy econ;
    private BukkitRunnable deductionTask;
    private double initialCost;
    private double recurringCost;
    private double killerPrize;
    private int costInterval;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        Configuration config = this.getConfig();
        this.initialCost = config.getDouble("initialCost", 1000);
        this.recurringCost = config.getDouble("recurringCost", 100);
        this.killerPrize = config.getDouble("killerPrize", 1000);
        this.costInterval = config.getInt("costInterval", 60 * 20);

        this.getLogger().info("Keeping your inventory safe and the server's wallet safer.");
        Bukkit.getPluginManager().registerEvents(this, this);

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        econ = rsp.getProvider();

        this.getCommand("insure").setExecutor(new CommandInsure(this, true));
        this.getCommand("uninsure").setExecutor(new CommandInsure(this, false));

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
            new InsurancePlaceholder(this).register();

        final InsureInventory that = this;
        this.deductionTask = new BukkitRunnable() {
            @Override
            public void run(){
                that.deductInsurancePremiums();
            }
        };

        if (this.costInterval != 0) {
            this.deductionTask.runTaskTimer(this, 0, this.costInterval);
        }
    }

    @Override
    public void onDisable() {
        this.deductionTask.cancel();
    }

    public void deductInsurancePremiums() {
        if (this.recurringCost == 0) return;
        for(Player player: this.getServer().getOnlinePlayers()) {
            if (this.isInsured(player)) {
                double bal = econ.getBalance(player);
                if (bal < this.recurringCost) {
                    player.sendMessage(
                        ChatColor.DARK_RED +
                        "You don't have enough cash to cover your insurance premium. " +
                        "Your inventory has been uninsured"
                    );
                    this.setInsured(player, false);
                    return;
                }

                econ.withdrawPlayer(player, this.recurringCost);
                player.sendMessage(String.format(
                    "Insurance premium withdrawn, %s$%s%s. Cancel with %s/uninsure%s.",
                    ChatColor.DARK_RED,
                    this.recurringCost,
                    ChatColor.WHITE,
                    ChatColor.GOLD,
                    ChatColor.WHITE
                ));
            }
        }
    }


    /**
     * Checks if a player is insured
     * @param player
     * @return if the player has active insurance
     */
    public boolean isInsured(Player player) {
        List<MetadataValue> insuranceHistory = player.getMetadata("insureInventory_insured");
        if (insuranceHistory.size() == 0) return false;
        boolean insured = insuranceHistory.get(insuranceHistory.size()-1).asBoolean();
        return insured;
    }

    /**
     * Marks a player as insured
     * @param player
     * @param insured
     */
    public void setInsured(Player player, Boolean insured) {
        FixedMetadataValue value = new FixedMetadataValue(this, insured);
        player.setMetadata("insureInventory_insured", value);
    }

    /**
     * Clears the drops and sets the keepInventory flag on an insured player's death event
     * @param event
     */
    @EventHandler
    public void onEntityDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        if (!this.isInsured(player)) {
            player.sendMessage("Your inventory was not insured and was dropped.");
            return;
        }

        if (this.killerPrize > 0) {
            Player killer = player.getKiller();
            if (killer != null) {
                this.econ.depositPlayer(killer, this.killerPrize);
                killer.sendMessage(String.format(
                    "%s%s%s's inventory was insured. You've been awarded %s$%s %sinstead.",
                    ChatColor.DARK_AQUA,
                    player.getName(),
                    ChatColor.WHITE,
                    ChatColor.GREEN,
                    this.killerPrize,
                    ChatColor.WHITE
                ));
            }
        }

        event.setKeepInventory(true);
        event.setKeepLevel(true);
        event.getDrops().clear(); // prevent deathchest from duplicating drops

        player.sendMessage(
            ChatColor.DARK_GREEN +
            "Your inventory was insured and kept after death. " +
            "You'll need to renew your insurance."
        );
        this.setInsured(player, false);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        this.setInsured(event.getPlayer(), false);
    }

    public Economy getEcon() {
        return this.econ;
    }

    public double getInitialCost() {
        return this.initialCost;
    }

    public double getRecurringCost() {
        return this.recurringCost;
    }
}
