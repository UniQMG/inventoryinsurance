package uniqmg.insureinventory;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

public class CommandInsure implements CommandExecutor {
    private InsureInventory plugin;
    private Boolean enable;

    public CommandInsure(InsureInventory plugin, Boolean enable) {
        this.plugin = plugin;
        this.enable = enable;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (this.enable) {
                Economy econ = this.plugin.getEcon();
                double cost = this.plugin.getInitialCost();

                if (econ.getBalance(player) < cost) {
                    player.sendMessage(String.format(
                        "%sNot enough money, %s$%s %sneeded.",
                        ChatColor.DARK_RED,
                        ChatColor.DARK_GREEN,
                        cost,
                        ChatColor.DARK_RED
                    ));
                    return true;
                }
                econ.withdrawPlayer(player, cost);
            }

            this.plugin.setInsured(player, this.enable);

            if (this.enable) {
                player.sendMessage(ChatColor.GOLD + "Inventory insured");
            } else {
                player.sendMessage(ChatColor.YELLOW + "Inventory uninsured");
            }
        }
        return true;
    }
}
